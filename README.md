# exchange #

Tests : No tests as its a spike.

## Build & Run ##

$ cd exchange
$ play run

Select country from drop down. Graph is created.

## Code walk through ##

Application.java

Majority of work is in here.

Spent longer than I would have liked parsing the XML in Java. This should be easy right?! Come on Java.
I made a decision to to use google guava to make some use of closures to filter out dates greater then 90 days.
Introduced indexAsJson for the ajax update. Obviously violating DRY and most likely not the play way. But as it was a spike and I was timeboxing this, i didn't sweat it.

d3graph.js

simple singleton javascript class to create a d3 graph.

index.html

Worked with 5 currencies for now. Couldn't find a link with all the countries.

Didn't want to inline the javascript, but wasn't sure how to use the jsRoute from a javascript file. Play was giving out.
This is triggered from the index.html template.

That's it :)
