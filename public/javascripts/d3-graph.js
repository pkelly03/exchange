var d3Graph = (function () {

    var instance;

    function init() {

        var MARGIN = [80, 80, 80, 80]; // margins
        var WIDTH = 1000 - MARGIN[1] - MARGIN[3]; // width
        var HEIGHT = 400 - MARGIN[0] - MARGIN[2]; // height

        var rates = [];
        var graph = null;
        var line = null;
        var x, y;


        function initializeGraphObject() {
            graph = d3.select("#graph").append("svg:svg")
                .attr("width", WIDTH + MARGIN[1] + MARGIN[3])
                .attr("height", HEIGHT + MARGIN[0] + MARGIN[2])
                .append("svg:g")
                .attr("transform", "translate(" + MARGIN[3] + "," + MARGIN[0] + ")");
        }

        function setupXandYAxis() {

            var minValue = _.min(rates,function (rate) {
                    return rate.value;
                }).value,
                maxValue = _.max(rates,function (rate) {
                    return rate.value;
                }).value,
                minDate = new Date(rates[0].date),
                maxDate = new Date(rates[rates.length - 1].date);

            x = d3.time.scale().domain([minDate, maxDate]).range([0, WIDTH]),
                y = d3.scale.linear().domain([ minValue - (.1 * minValue) , maxValue + (.1 * maxValue) ]).range([HEIGHT, 0]);

            var yAxis = d3.svg.axis()
                .scale(y)
                .orient("left")
                .ticks(5);

            var xAxis = d3.svg.axis()
                .scale(x)
                .orient("bottom")
                .ticks(5);


            graph.append("g")
                .attr("class", "axis")
                .call(yAxis);

            graph.append("g")
                .attr("class", "axis")
                .attr("transform", "translate(0," + HEIGHT + ")")
                .call(xAxis);

            line = d3.svg.line()
                .x(function (d) {
                    console.log('Plotting X value for data point: ' + d + 'using our xScale.');
                    return x(new Date(d["date"]));
                })
                .y(function (d) {
                    console.log('Plotting Y value for data point: ' + d + ' to be at: ' + y(d["value"]) + " using our yScale.");
                    return y(d["value"]);
                })

        }

        function drawLineChart() {
            graph.append("svg:path")
                .attr("d", line(rates));
        }

        return {

            render: function (args) {
                if (_.isArray(args)) {
                    rates = args;
                } else {
                    rates = JSON.parse(_.unescape(args));
                }
                initializeGraphObject();
                setupXandYAxis();
                drawLineChart();
            }
        }


    };

    return {

        getInstance: function () {
            if (!instance) {
                instance = init();
            }
            return instance;
        }
    }
})();