package controllers;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import models.ExchangeRate;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import play.mvc.Controller;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.util.ArrayList;
import java.util.List;

import play.libs.WS;

public class Application extends Controller {

    public static final int NINETY_DAYS = 90;

    public static void index(String country) {

        country = (country == null) ? "usd" : country;

        try {
            String url = "http://www.ecb.int/stats/exchange/eurofxref/html/" + country + ".xml";
            WS.HttpResponse httpResponse = WS.url(url).get();

            DocumentBuilderFactory factory =
                    DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();

            Document document = builder.parse(httpResponse.getStream());
            document.getDocumentElement().normalize();

            Gson gson = new GsonBuilder().create();

            String ratesAsJson = gson.toJson(mapToModel(filterLastNinetyDays(getObsElements(document))));
            render(ratesAsJson);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void indexAsJson(String country) {

        country = (country == null) ? "usd" : country;

        try {
            String url = "http://www.ecb.int/stats/exchange/eurofxref/html/" + country + ".xml";
            WS.HttpResponse httpResponse = WS.url(url).get();

            DocumentBuilderFactory factory =
                    DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();

            Document document = builder.parse(httpResponse.getStream());
            document.getDocumentElement().normalize();

            renderJSON(mapToModel(filterLastNinetyDays(getObsElements(document))));

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static List<ExchangeRate> mapToModel(ArrayList<Element> rates) {
        List<ExchangeRate> toModel = new ArrayList();
        for (Element element : rates) {
            ExchangeRate exchangeRate = new ExchangeRate(element.getAttribute("TIME_PERIOD"), Double.valueOf(element.getAttribute("OBS_VALUE")));
            toModel.add(exchangeRate);
        }
        return toModel;
    }

    private static ArrayList filterLastNinetyDays(List obsElements) {

        Predicate<Element> lastNinetyDays = new Predicate<Element>() {
            public boolean apply(final Element element) {
                LocalDate now = new LocalDate();
                LocalDate timePeriod = new LocalDate(element.getAttribute("TIME_PERIOD"));
                int days = Days.daysBetween(timePeriod, now).getDays();
                return (days <= NINETY_DAYS) ? true : false;
            }
        };

        return Lists.newArrayList(Iterables.filter(obsElements, lastNinetyDays));
    }

    private static List<Element> getObsElements(Document document) {

        List<Element> fullObsList = new ArrayList<Element>();

        NodeList nList = document.getElementsByTagName("DataSet");

        Node item = nList.item(0);
        if (item.getNodeType() == Node.ELEMENT_NODE) {
            Element series = (Element) item;
            NodeList seriesList = series.getElementsByTagName("Series");
            Node seriesNode = seriesList.item(0);
            NodeList obsList = seriesNode.getChildNodes();
            int obsLength = obsList.getLength();
            for (int i = 0; i < obsLength; i++) {
                Node obsNode = obsList.item(i);
                if (obsNode.getNodeType() == Node.ELEMENT_NODE) {
                    fullObsList.add((Element) obsNode);
                }
            }
        }
        return fullObsList;
    }

}